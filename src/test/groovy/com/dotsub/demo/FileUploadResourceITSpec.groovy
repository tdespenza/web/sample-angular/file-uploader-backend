package com.dotsub.demo

import com.dotsub.demo.model.FileMetadata
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.io.ClassPathResource
import org.springframework.http.*
import org.springframework.test.context.ContextConfiguration
import org.springframework.util.LinkedMultiValueMap
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Unroll

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/13/16.
 */
@Narrative("""
    As a user
    I should be able to upload files
    And store there metadata
    So that I can view it at a later date
""")
@ContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FileUploadResourceITSpec extends Specification {

    @Autowired
    TestRestTemplate template

    @Unroll
    def "testing upload with #filename"(final String filename, final String contentType) {
        setup:
        final LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file", new ClassPathResource("files/$filename"));
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        final HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);

        when:
        final ResponseEntity entity = template.postForEntity("/api/upload", requestEntity, Void.class)

        then:
        final FileMetadata metadata = template.getForEntity("/api/metadata/" + filename, FileMetadata.class)?.body

        expect:
        // did it store it successfully?
        entity.statusCode == HttpStatus.OK

        // did it really save to the db?
        println metadata
        metadata?.title == filename
        assert metadata?.description
        assert metadata?.creationDate

        where:
        filename         | contentType
        "birds.jpg"      | MediaType.IMAGE_JPEG_VALUE
        "city.jpg"       | MediaType.IMAGE_JPEG_VALUE
        "sample.3gp"     | "video/3gpp"
        "sample.flv"     | "video/x-flv"
        "sample.mkv"     | "video/x-matroska"
        "sample.mp4"     | "video/mp4"
        "testfile.txt"   | MediaType.TEXT_PLAIN_VALUE
        "tiger.jpg"      | MediaType.IMAGE_JPEG_VALUE
        "turpentine.pdf" | "application/pdf"
    }
}
