package com.dotsub.demo.rest;

import com.dotsub.demo.exception.StorageFileNotFoundException;
import com.dotsub.demo.model.FileMetadata;
import com.dotsub.demo.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/12/16.
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class FileUploadResource {

    @Autowired
    private StorageService storageService;

    @PostMapping(value = "/upload")
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file) {
        storageService.store(file);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/metadata")
    public ResponseEntity<List<FileMetadata>> listMetadataCollection() {
        return ResponseEntity
                .ok()
                .body(storageService.getMetadataCollection());
    }

    @GetMapping("/metadata/{filename:.+}")
    public ResponseEntity<FileMetadata> listMetadata(@PathVariable final String filename) {
        return ResponseEntity
                .ok()
                .body(storageService.getMetadata(filename));
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity handleFileNotFound(final StorageFileNotFoundException exception) {
        return ResponseEntity.notFound().build();
    }
}
