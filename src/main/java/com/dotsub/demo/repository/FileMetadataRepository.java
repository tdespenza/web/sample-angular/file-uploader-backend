package com.dotsub.demo.repository;

import com.dotsub.demo.domain.FileMetadataEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/12/16.
 */
@Repository
public interface FileMetadataRepository extends CrudRepository<FileMetadataEntity, Long> {
    FileMetadataEntity findByTitle(final String title);
}
