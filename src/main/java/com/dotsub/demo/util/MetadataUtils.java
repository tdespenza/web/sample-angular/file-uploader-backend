package com.dotsub.demo.util;

import com.dotsub.demo.exception.StorageException;
import lombok.extern.java.Log;
import lombok.val;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/14/16.
 */
@Log
public class MetadataUtils {
    public static Metadata extractMetadata(final InputStream inputStream, final String filename) {
        try {
            final Metadata metadata = new Metadata();
            final Parser parser = new AutoDetectParser();
            parser.parse(inputStream, new BodyContentHandler(), metadata, new ParseContext());

            return metadata;

        } catch (final SAXException | TikaException | IOException e) {
            throw new StorageException("Couldn't extract metadata from " + filename, e);
        }
    }

    public static LocalDateTime extractCreationDate(final String datetime, final MultipartFile multipartFile) throws IOException {
        if (!StringUtils.isEmpty(datetime)) return LocalDateTime.parse(datetime);

        val path = Files.createTempFile("temp-", "." + FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
        multipartFile.transferTo(path.toFile());
        val attributes = Files.readAttributes(path, BasicFileAttributes.class);
        return LocalDateTime.ofInstant(attributes.creationTime().toInstant(), ZoneId.systemDefault());
    }
}
