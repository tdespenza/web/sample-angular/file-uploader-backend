package com.dotsub.demo.domain;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/12/16.
 */

@Data
@Entity(name = "FILE_METADATA")
public class FileMetadataEntity implements Serializable {
    private static final long serialVersionUID = -6144844158058519354L;
    @Id
    @GeneratedValue
    @Column(updatable = false, nullable = false)
    private Long id;
    @NonNull
    @Column(nullable = false, unique = true)
    private String title;
    @NonNull
    @Column(nullable = false)
    private String description;
    @NonNull
    @Column(nullable = false)
    private LocalDateTime creationDate;

    public FileMetadataEntity() {
    }

    public FileMetadataEntity(final String title, final String description, final LocalDateTime creationDate) {
        this.title = title;
        this.description = description;
        this.creationDate = creationDate;
    }
}
