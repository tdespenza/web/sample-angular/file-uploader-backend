package com.dotsub.demo.service;

import com.dotsub.demo.config.StorageProperties;
import com.dotsub.demo.domain.FileMetadataEntity;
import com.dotsub.demo.exception.StorageException;
import com.dotsub.demo.model.FileMetadata;
import com.dotsub.demo.repository.FileMetadataRepository;
import com.dotsub.demo.util.MetadataTranslateUtils;
import lombok.extern.java.Log;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.List;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/12/16.
 */
@Log
@Service
public class StorageService {
    private final Path uploadDirectory;

    @Autowired
    private FileMetadataRepository fileMetadataRepository;

    public StorageService(final StorageProperties storageProperties) {
        this.uploadDirectory = Paths.get(storageProperties.getUploadDir());
    }

    public void store(final MultipartFile file) {
        if (file.isEmpty()) {
            throw new StorageException("Couldn't store file " + file.getOriginalFilename());
        }

        try {
            val inputStream = file.getInputStream();
            val filename = file.getOriginalFilename();
            val path = uploadDirectory.resolve(filename);

            Files.copy(inputStream, path, StandardCopyOption.REPLACE_EXISTING);
            val translatedEntity = MetadataTranslateUtils.tranlateToEntity(file);
            FileMetadataEntity entity = fileMetadataRepository.findByTitle(filename);

            if (entity == null) {
                entity = translatedEntity;

            } else {
                entity.setDescription(translatedEntity.getDescription());
                entity.setCreationDate(translatedEntity.getCreationDate());
            }

            fileMetadataRepository.save(entity);

        } catch (final IOException e) {
            throw new StorageException("Couldn't store file " + file.getOriginalFilename(), e);
        }
    }

    public List<FileMetadata> getMetadataCollection() {
        val resources = new LinkedList<FileMetadata>();

        fileMetadataRepository.findAll().forEach(entity -> resources.add(MetadataTranslateUtils.translateFromEntity(entity)));

        return resources;
    }

    public FileMetadata getMetadata(final String filename) {
        return MetadataTranslateUtils.translateFromEntity(fileMetadataRepository.findByTitle(filename));
    }

    public void init() {
        try {
            Files.createDirectory(uploadDirectory);

        } catch (final IOException e) {
            throw new StorageException("Couldn't create root directory: " + uploadDirectory, e);
        }
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(uploadDirectory.toFile());
    }
}
